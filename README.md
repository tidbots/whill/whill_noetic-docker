# WHILL Noetic Docker
電動車椅子WHILLをDockerで動かす

電動車椅子WHILLをROS Noeticで動かしてきましたが、手持ちのPCがUbuntu22.04に移行するのに従って、ROS Noeticの環境が少なくなってきました。

そこで、Ubuntu22.04でもWHILLを動かせるようにDocker環境を作りました。


## To Do
17 Jun. 2024

## テストしたホストPC環境
- Ubuntu 22.04 LTS
- NVIDIA
- Docker

[ROG Flow Z13-ACRNM](https://rog.asus.com/jp/laptops/rog-flow/rog-flow-z13-acrnm-rmt02/spec/)
- インテル® Core™ i9-13900H プロセッサー
- 32 GB RAM
- NVIDIA® GeForce RTX™ 4070 Laptop GPU (NVIDIA® Optimus™ Technology対応)
ビデオメモリ 8GB


## 準備
### ハードウェア
[]()を参照してください。

### WHILL本体、LiDAR(URG)、ゲームパッドのデバイス名を固定する

シリアルポート（USB）に挿す順番でデバイス名が変わると困るので、デバイス名を固定する。

下記のコマンドでUSBに刺さっているデバイスの一覧を取得する
```
(host PC) lssub

```
- WHILL本体
- URG<br>
Bus 002 Device 007: ID 15d1:0000
- ゲームパッド

```
(host PC) sudo vi /etc/udev/rules.d/99-serial.rules
...
...
KERNEL=="ttyACM*", ATTRS{idVendor}=="15d1", ATTRS{idProduct}=="0000", SYMLINK+="ttyACM_URG", MODE="0666"
```
確認する
```
(host PC) ls -la /dev/ttyACM*
crw-rw-rw- 1 root dialout 166, 0 4月 9 12:29 /dev/ttyACM0
lrwxrwxrwx 1 root root 7 4月 9 12:29 /dev/ttyACM_URG -> ttyACM0
```


## リポジトリのクローン
```
(host pc) cd ~
(host pc) git clone https://gitlab.com/tidbots/whill/whill_noetic-docker.git
```

## Dockerイメージの作成
```
(host pc) cd ~/whill_noetic-docker
(host pc) docker compose build
```

## Dockerコンテナの起動
```
(host pc) cd ~/whill_noetic-docker
(host pc) docker compose up
```

## Whillを動かしてみる


# Authors
Hiroyuki Okada

# Contact
admin@okadanet.org

# LICENSE
このプロジェクトは、MIT Licenseの下でライセンスされています。 詳しく見る
