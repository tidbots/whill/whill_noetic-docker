# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM nvidia/cudagl:11.3.0-devel-ubuntu20.04

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="June 4, 2024"

SHELL ["/bin/bash", "-c"]
ARG DEBIAN_FRONTEND=noninteractive

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
ENV __NV_PRIME_RENDER_OFFLOAD=1
ENV __GLX_VENDOR_LIBRARY_NAME=nvidia
ARG ROS_DISTRO=noetic

# Timezone, Launguage Setting
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe

# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo

# install packages
RUN apt-get update && apt-get install -q -y --no-install-recommends \
  git build-essential  iputils-ping net-tools \
  cmake  g++ \
  iproute2 gnupg gnupg1 gnupg2 \
  libcanberra-gtk*   python3-pip   python3-tk \
  git wget curl sudo python-is-python3 \
  mesa-utils x11-utils x11-apps terminator xterm xauth \
  terminator xterm nano vim htop \
  software-properties-common gdb valgrind sudo \
  ca-certificates language-pack-ja locales fonts-takao \
  usbutils \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install ROS1 noetic
RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
RUN apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
RUN apt-get update && apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages \
libpcap-dev \
libopenblas-dev \
gstreamer1.0-tools libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev \
ros-noetic-desktop-full \
python3-rosdep python3-rosinstall-generator python3-vcstool build-essential \
ros-noetic-socketcan-bridge \
ros-noetic-geodesy   \
python3-catkin-tools \
ros-noetic-joy ros-noetic-teleop-twist-joy ros-noetic-urg-node ros-noetic-serial  \
ros-noetic-gmapping libbullet-dev libsdl-image1.2-dev libsdl-dev ros-noetic-navigation \ 
ros-noetic-geometry2 \
ros-noetic-usb-cam ros-noetic-image-view  && \
apt-get clean && rm -rf /var/lib/apt/lists/*
RUN rosdep init
#RUN rosdep update

# Install tmux 3.2(if you wish)
RUN apt-get update && apt-get install -y automake autoconf pkg-config libevent-dev libncurses5-dev bison && \
apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/tmux/tmux.git && \
cd tmux && git checkout tags/3.2 && ls -la && sh autogen.sh && ./configure && make -j8 && make install


COPY assets/entrypoint.sh /tmp/entrypoint.sh
RUN  chmod a+x /tmp/entrypoint.sh

# Create an overlay Catkin workspace
RUN source /opt/ros/noetic/setup.bash \
 && mkdir -p /overlay_ws/src \
 && cd /overlay_ws/src \ 
 && catkin_init_workspace \ 
 && git clone https://github.com/WHILL/ros_whill.git \
 && git clone https://gitlab.com/tidbots/whill/whill_noetic.git  \
 && cd /overlay_ws \
 && rosdep update \
 && rosdep install --from-paths src --ignore-src -r -y \
 && catkin_make

# Add user and group
ARG ROBOT_NAME
ARG UID
ARG GID
ARG USER_NAME
ARG GROUP_NAME
ARG PASSWORD
ARG NETWORK_IF
ARG WORKSPACE_DIR
ARG ROBOT_NAME

RUN groupadd -g $GID $GROUP_NAME && \
    useradd -m -s /bin/bash -u $UID -g $GID -G sudo $USER_NAME && \
    echo $USER_NAME:$PASSWORD | chpasswd && \
    echo "$USER_NAME   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
USER ${USER_NAME}

# Config
RUN mkdir -p ~/.config/terminator/
COPY assets/terminator_config /home/$USER_NAME/.config/terminator/config 


#RUN echo "export ROS_HOME=~/.ros" >> ~/.bashrc
#RUN echo "export ROBOT_NAME=${ROBOT_NAME}" >> ~/.bashrc
RUN echo "export ROS_MASTER_URI=http://${ROBOT_NAME}.local:11311" >> ~/.bashrc
RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN echo "source /overlay_ws/devel/setup.bash" >> ~/.bashrc
RUN echo "export MESA_LOADER_DRIVER_OVERRIDE=i965" >> ~/.bashrc
#RUN rosdep update

ENTRYPOINT [ "/tmp/entrypoint.sh" ]
CMD ["/bin/bash"]
