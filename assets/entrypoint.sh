#!/bin/bash
set -e
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
source /opt/ros/noetic/setup.bash
source /overlay_ws/devel/setup.bash

exec "$@"
